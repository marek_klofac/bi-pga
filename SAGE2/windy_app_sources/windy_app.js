//
// SAGE2 application: Windy App
// by: Marek Klofac (klofama1@fit.cvut.cz)
//
// Whole code is heavily inspired by other SAGE 2 application,
// that is commonly accesbile via the SAGE 2 server (one of the default apps).
//
// App name: Google Maps for SAGE 2
// Giving credits to:
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//

"use strict";

/* global  */

var MOUSE_SENSITIVITY = 7;

var windy_app = SAGE2_App.extend(
{
	init: function(data)
	{
		// Create div into the DOM
		this.SAGE2Init("div", data);
		this.element.id = "div_" + data.id;

		this.resizeEvents = "continuous";
		this.maxFPS = 10;

		// Not adding controls but making the default buttons available
		this.controls.finishedAddingControls();
		this.enableControls = true;

		// Useful variables
		this.dragging     = false;
		this.position     = {x: 0, y: 0};
		this.scrollAmount = 0; 

		// Initialize widgets
		this.controls.addButton({type: "zoom-in", position: 5, identifier: "zoomIn"});
		this.controls.addButton({type: "zoom-out", position: 13, identifier: "zoomOut"});
		this.layerType = this.controls.addRadioButton({identifier: "layerType",
			label: "Layer Type",
			options: ['wind', 'rain', 'temp', 'clouds', 'waves', 'currents', 'pressure'],
			default: "wind"
		});

		this.controls.finishedAddingControls();

		// Create div for Windy to draw to
		this.windy_div = document.createElement("div");
		this.windy_div.id = "windy";
		this.windy_div.style.width = "100%";
		this.windy_div.style.height = "100%";
		this.element.appendChild(this.windy_div);

		// Importing other .js scripts (needed for Leaflet and Windy API)
		this.dynamicallyLoadScript("https://unpkg.com/leaflet@1.4.0/dist/leaflet.js")
		.then(() => this.dynamicallyLoadScript("https://api4.windy.com/assets/libBoot.js"))
		.then(() =>
		{
			const options =
			{
			    // Required API key
			    key: 'QgpDhyBVKFjRRqGqOeCZRVgiGUjOFhLb', // REPLACE WITH YOUR KEY !!!

			    // Put additional console output
			    verbose: true,

			    // Initial state of the map -> Using sync variables from instructions.json
			    lat: this.state.center.lat,
			    lng: this.state.center.lng,
			    zoom: this.state.zoom,
			};
			// Initialize Windy API
			windyInit(options, windyAPI =>
			{
			    // windyAPI is ready, and contain 'map', 'store', 'picker' and other usefull stuff
			    this.windyAPI = windyAPI;
			    // Turning off Windy API animations, because of high performance issues
			    this.windyAPI.store.set('particlesAnim', 'off')
			    this.map = windyAPI.map;
			});
		})
	},

	load: function(date)
	{
		this.refresh(date);
	},

	draw: function(date)
	{
	
	},

	resize: function(date)
	{
		// Updating newly calculated map center
		this.updateMapCenter();
		// Forcing leaflet map to check and resize to newly resized SAGE 2 window
		this.map.invalidateSize();
		this.map.setView(this.state.center, this.state.zoom);
		this.refresh(date);
	},

	move: function(date)
	{
		// Called when window is moved (set moveEvents to continuous)
		this.refresh(date);
	},

	quit: function()
	{
		// Make sure to delete stuff (timers, ...)
	},

	// Handling all kinds of input (mouse, keyboard, etc.)
	event: function(eventType, position, user_id, data, date)
	{
		// Right mouse button pressed
		if (eventType === "pointerDblClick")
		{
			// Transform where user clicked to lattitude and longitude
			var pos = this.map.containerPointToLatLng(position);
			this.windyAPI.broadcast.fire('rqstOpen', 'picker', { lat: pos.lat, lon: pos.lng });
			this.refresh(date);
		}
		// Left mouse button pressed
		if (eventType === "pointerPress" && (data.button === "left"))
		{
			this.dragging = true;
			this.position.x = position.x;
			this.position.y = position.y;
			this.refresh(date);
		}
		// Left mouse button move while being pressed
		else if (eventType === "pointerMove" && this.dragging)
		{
			// Move map to cursor's current location
			// Moves it in direction by number of given pixels
			// (subtracting previous and current position of cursor * Mouse sensitivity)
			this.map.panBy([MOUSE_SENSITIVITY*(this.position.x - position.x),
							MOUSE_SENSITIVITY*(this.position.y - position.y)]);
			this.updateMapCenter();
			this.position.x = position.x;
			this.position.y = position.y;
			this.refresh(date);
		}
		// Left mouse button released
		else if (eventType === "pointerRelease" && (data.button === "left"))
		{
			this.dragging = false;
			this.position.x = position.x;
			this.position.y = position.y;
			this.map.setView(this.state.center, this.state.zoom);
			this.refresh(date);
		}
		// Middle mouse scroll event
		else if (eventType === "pointerScroll")
		{
			this.scrollAmount = data.wheelDelta;

			if (this.scrollAmount > 0)
			{
				// Zoom out
				var z = this.map.getZoom();
				this.map.setZoom(z - 1);
				this.state.zoom = this.map.getZoom();
			}
			else if (this.scrollAmount < 0)
			{
				// Zoom in
				var z = this.map.getZoom();
				this.map.setZoom(z + 1);
				this.state.zoom = this.map.getZoom();
			}
			this.refresh(date);
		}
		// Right mouse button pressed (widget menu appears)
		else if (eventType === "widgetEvent")
		{
			switch (data.identifier)
			{
				// Changing the map overlay dependent on input (where clicked)
				case "layerType":
					this.state.mapType = data.value
					this.windyAPI.store.set('overlay', data.value);
					break;
				// Zooming in without wheel button
				case "zoomIn":
					var z = this.map.getZoom();
					this.map.setZoom(z + 1);
					this.state.zoom = this.map.getZoom();
					break;
				// Zooming out without wheel button
				case "zoomOut":
					var z = this.map.getZoom();
					this.map.setZoom(z - 1);
					this.state.zoom = this.map.getZoom();
					break;
				// Other unspecified data identifiers
				default:
					console.log("windy_app> No handler for: " + data.identifier + "->" + data.action);
					break;
			}
			this.refresh(date);
		}
		// Keyboard button pressed
		else if (eventType === "specialKey")
		{
			// Left arrow
			if (data.code === 37 && data.state === "down")
			{
				this.map.panBy([-100, 0]);
			}
			// Up arrow
			else if (data.code === 38 && data.state === "down")
			{
				this.map.panBy([0, -100]);
			}
			// Right arrow
			else if (data.code === 39 && data.state === "down")
			{
				this.map.panBy([100, 0]);
			}
			// Down arrow
			else if (data.code === 40 && data.state === "down")
			{
				this.map.panBy([0, 100]);
			}
			this.updateMapCenter();
			this.refresh(date);
		}
	},

	// Updating state values for synchronization
	updateMapCenter: function()
	{
		// As master, calculate new map center and send to clients
		if(isMaster)	
		{
			var center = this.map.getCenter();
			var zoom = this.map.getZoom();
			this.state.center = {lat: center.lat, lng: center.lng};
			this.state.zoom = zoom;
			this.broadcast("syncStateAcrossClients", {center: center, zoom: zoom});
		}
	},

	// Syncing map center
	syncStateAcrossClients: function(data)
	{
		this.state.center = data.center;
		this.state.zoom = data.zoom;
	},

	// Source: https://stackoverflow.com/questions/950087/how-do-i-include-a-javascript-file-in-another-javascript-file
	// Author: Josh Habdas
	//
	// Linking other .js scripts given URL
	// Here used for Windy API .js files and Leaflet API
	dynamicallyLoadScript: function(url)
	{
	    return new Promise(function(resolve, reject)
	    {
	        var script = document.createElement("script");
	        script.src = url;
	        script.onload = resolve;
	        script.onerror = () => reject(new Error(`Error when loading ${url}!`));
	        document.body.appendChild(script);
	    })
	}
});

