#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Tato semestralni prace je inspirovana kodem jineho studenta predmetu BI-PGA.
# Nektere funkce jsou zkopirovany primo z jeho prace, nektere jsou vsak upraveny
# Tento plugin pridava hlavne lepsi UI a Memory management v ramci Tile-based processingu obrazku
#
# Zdroj: https://gitlab.fit.cvut.cz/BI-PGA/stud/zadinvit/blob/master/doc1/konvoluce2_zadina.py

import gimp, gimpplugin, math, array
from gimpenums import *
pdb = gimp.pdb
import gtk, gimpui, gimpcolor
from gimpshelf import shelf
import cv2
import numpy as np
#..........................................................................................................................................#
class convolution_plugin(gimpplugin.plugin):

    def start(self):
        gimp.main(self.init, self.quit, self.query, self._run)

    def init(self):
        pass

    def quit(self):
        pass

    def query(self):
        gimp.install_procedure(
            "convolution_plugin_main",
            "Convolution",
            "Convolution",
            "Marek Klofac",
            "Marek Klofac",
            "2019",
            "<Image>/Filters/Convolution",
            "RGB*",
            PLUGIN,
            [
                # next three parameters are common for all scripts that are inherited from gimpplugin.plugin
                (PDB_INT32, "run_mode", "Run mode"),
                (PDB_IMAGE, "image", "Input image"),
                (PDB_DRAWABLE, "drawable", "Input drawable"),
                # plugin specific parameters
                (PDB_INT32, "radius", "Value of radius"),
                (PDB_INT32, "pixelate_factor", "Value of pixelate factor"),
                (PDB_INT32, "tile_optimalization", "Turn on/off processing tile by tile"),
                (PDB_INT32, "filter", "Filter mode."),
            ],
            []
        )

    def convolution_plugin_main(self, run_mode, image, drawable, radius=1, pixelate_factor=1, tile_optimalization=False, filter=0):
        # Important class variables and parameters
        self.image = image
        self.drawable = drawable
        self.radius = radius
        self.pixelate_factor = pixelate_factor
        self.tile_optimalization = tile_optimalization
        self.operations = ["Gaussian sharpen", "Gaussian blur", "Motion Horizontal Blur", "Motion Vertical Blur", "Motion Diagonal Blur", "Pixelate"]
        self.selected_operation = self.operations[filter]
        self.layer = None

        pdb.gimp_image_undo_group_start(self.image)
        self.create_dialog()
        self.dialog.run()
        pdb.gimp_image_undo_group_end(self.image)
        gimp.displays_flush()

    # Source: https://gitlab.fit.cvut.cz/BI-PGA/stud/zadinvit/blob/master/doc1/konvoluce2_zadina.py
    # Set matrix a*a where a=(2*radius) and one of middle row with value=1/size
    def set_horizontal_matrix(self):
        size = (2 * int(self.radius))
        matrix = np.zeros((size, size))
        # filling middle row with 1
        matrix[int((size - 1) / 2), :] = np.ones(size)
        matrix = matrix/size
        return matrix

    # Source: https://gitlab.fit.cvut.cz/BI-PGA/stud/zadinvit/blob/master/doc1/konvoluce2_zadina.py
    # Set matrix axa where a=(2*radius) and one of middle column with value=1/size
    def set_vertical_matrix(self):
        size = (2 * int(self.radius))
        matrix = np.zeros((size, size))
        #filling middle column with 1
        matrix[:,int((size - 1) / 2)] = np.ones(size)
        matrix = matrix / size
        return matrix

    # Source: https://gitlab.fit.cvut.cz/BI-PGA/stud/zadinvit/blob/master/doc1/konvoluce2_zadina.py
    # Set kernel matrix to size a x a, where a=(2*radius) fill diagonal with value 1/a
    def set_diagonal_matrix(self):
        size = (2 * int(self.radius))
        matrix = np.zeros(shape=(size, size))

        np.fill_diagonal(matrix, 1, wrap=True)
        matrix = matrix / size
        return matrix

    # Applying filter to specified region by X and Y coordinates
    def apply_filter(self, x1, x2, y1, y2):
        # DEBUG: printing the current region that is selected for filter operations
        # print("------------------------")
        # print("Working on region:")
        # print("Horizontal: " + str(x1) + " -> " + str(x2))
        # print("Vertical: " + str(y1) + " -> " + str(y2))

        width = x2 - x1
        height = y2 - y1

        # Taking all pixels from source
        src_pixels = array.array("B", self.src_rgn[x1:x2, y1:y2])

        # Numpy representation of pixels
        pixel_data = np.reshape(src_pixels, (height, width, self.channel_count)).astype(np.uint8)

        # Destination region, where modified pixels are saved - into new layer
        dst_rgn = self.layer.get_pixel_rgn(x1, y1, x2, y2, True, True)

        # Doing filtering operation dependent on UI combobox value
        # Gaussian sharpen
        if (self.selected_operation == self.operations[0]):
            # Set matrix for filter
            kernel = np.array([[-1,-1,-1], [-1,9,-1], [-1,-1,-1]])
            pixel_data = cv2.filter2D(pixel_data,-1,kernel)

        # Gaussian blur
        if (self.selected_operation == self.operations[1]):
            # Using function from OpenCV library
            pixel_data = cv2.GaussianBlur(pixel_data, (0, 0), self.radius, self.radius, cv2.BORDER_DEFAULT)

        # Motion Horizontal Blur
        if (self.selected_operation == self.operations[2]):
            # Set matrix for filter
            kernel = self.set_horizontal_matrix()
            # Using function from OpenCV library
            pixel_data = cv2.filter2D(pixel_data, -1, kernel)

        # Motion Vertical Blur
        if (self.selected_operation == self.operations[3]):
            # Set matrix for filter
            kernel = self.set_vertical_matrix()
            # Using function from OpenCV library
            pixel_data = cv2.filter2D(pixel_data, -1, kernel)

        # Motion Diagonal Blur
        if (self.selected_operation == self.operations[4]):
            # Set matrix for filter
            kernel = self.set_diagonal_matrix()
            # Using function from OpenCV library
            pixel_data = cv2.filter2D(pixel_data, -1, kernel)

        # Pixelate
        if (self.selected_operation == self.operations[5]):
            # Using scale down and then back up method for pixelating
            target_width = int(width/self.pixelate_factor)
            target_height = int(height/self.pixelate_factor)
            pixel_data = cv2.resize(pixel_data, (target_width, target_height), interpolation=cv2.INTER_LINEAR)
            pixel_data = cv2.resize(pixel_data, (width, height), interpolation=cv2.INTER_NEAREST)

        # Putting modified pixel data to new layer
        dst_rgn[x1:x2, y1:y2] = pixel_data.tostring()

    # Function start filtring images
    def convolute(self):
        # Getting user's input to variables
        self.radius = self.scale_radius.get_value()
        self.pixelate_factor = self.scale_pixelate.get_value() 
        
        # Input can be RGB or RGBA
        self.channel_count = self.drawable.bpp
        (bx1, by1, bx2, by2) = self.drawable.mask_bounds
        source_width = bx2 - bx1
        source_height = by2 - by1

        # Whole source image (active)
        self.src_rgn = self.drawable.get_pixel_rgn(bx1, by1, source_width, source_height, False, False)
        gimp.progress_update(0.1)

        if self.layer is not None:
            self.image.remove_layer(self.layer)
            self.layer = None

        # Create appropriate "Preview" layer for filters
        if self.channel_count == 3: #RGB
            self.layer = gimp.Layer(self.image, "Preview", source_width, source_height, RGB_IMAGE, 100, NORMAL_MODE)
        elif self.channel_count == 4: # RGBA
            self.layer = gimp.Layer(self.image, "Preview", source_width, source_height, RGBA_IMAGE, 100, NORMAL_MODE)

        # Add newly created layer into image
        self.image.add_layer(self.layer, 0)
        gimp.progress_update(0.2)

        # Size of tile in pixels -> future upgrades: could be a parameter
        tile_size = 512

        # Processing picture by tiles (optimalization) if needed -> smaller images than tile_size processed as a whole
        if self.tile_optimalization and tile_size <= source_width and tile_size <= source_height:
            vertical_tile_offset = source_height % tile_size
            horizontal_tile_offset = source_width % tile_size
            vertical_full_tiles = (source_height - vertical_tile_offset)/tile_size
            horizontal_full_tiles = (source_width - horizontal_tile_offset)/tile_size

            # Processing only full tiles
            for y in range(vertical_full_tiles):
                for x in range(horizontal_full_tiles):
                    # Starting in top left corner going right line by line with tiles (x1, x2, y1, y2)
                    self.apply_filter(
                        x*tile_size,
                        (x+1)*tile_size,
                        y*tile_size,
                        (y+1)*tile_size
                    )
                # Gimp status bar ends at 80 %
                gimp.progress_update(((y+1)/float(vertical_full_tiles))-0.2)

    		    # Processing excessive horizontal border tiles (x1, x2, y1, y2)
                self.apply_filter(
                    horizontal_full_tiles*tile_size,
                    (horizontal_full_tiles*tile_size)+horizontal_tile_offset,
                    y*tile_size, 
                    (y+1)*tile_size
                )

    		# Processing excessive vertical border tiles (x1, x2, y1, y2)
            for x in range(horizontal_full_tiles):
                self.apply_filter(
                    x*tile_size,
                    (x+1)*tile_size,
                    vertical_full_tiles*tile_size,
                    (vertical_full_tiles*tile_size)+vertical_tile_offset
                )

            # Processing bottom right corner as last (x1, x2, y1, y2)
            self.apply_filter(
                horizontal_full_tiles*tile_size,
                (horizontal_full_tiles*tile_size)+horizontal_tile_offset,
                vertical_full_tiles*tile_size,
                (vertical_full_tiles*tile_size)+vertical_tile_offset
            )
            gimp.progress_update(0.9)

        # Processing picture as a whole -> plugin may crash if applied on large pictures
        else:
            self.apply_filter(0, source_width, 0, source_height)
            gimp.progress_update(0.9)

        # Refresh & save to layer
        self.layer.flush()
        self.layer.update(0, 0, source_width, source_height)
        self.layer.merge_shadow(True)
        gimp.displays_flush()
        gimp.progress_update(1.0)

    # UI creation
    def create_dialog(self):
        self.dialog = gimpui.Dialog("Convolution", "convolution_dialog")
        
        # 4x4 homogenous table
        self.table = gtk.Table(5, 4, False)
        self.table.set_row_spacings(8)
        self.table.set_col_spacings(8)
        self.table.show()

        # Dialog inner frames
        self.dialog.vbox.hbox1 = gtk.HBox(True, 0)
        self.dialog.vbox.hbox1.show()
        self.dialog.vbox.pack_start(self.dialog.vbox.hbox1, False, False, 0)
        self.dialog.vbox.hbox1.pack_start(self.table, True, True, 0)

        # Filter label
        self.label_filter = gtk.Label("Filter:")
        self.label_filter.show()
        self.table.attach(self.label_filter, 1, 2, 1, 2)

        # Drop down menu for choice of filters
        self.combobox_filter = gtk.combo_box_new_text()
        for operation in self.operations:
            self.combobox_filter.append_text(operation)
        self.combobox_filter.connect("changed", self.combobox_on_change)
        self.combobox_filter.set_entry_text_column(0)
        self.combobox_filter.set_active(0)
        self.combobox_filter.show()
        self.table.attach(self.combobox_filter, 2, 3, 1, 2)

        # Radius label
        self.label_radius = gtk.Label("Radius:")
        self.label_radius.show()
        self.table.attach(self.label_radius, 1, 2, 2, 3)
        self.label_radius.set_visible(False)

        # Radius scale
        self.adj = gtk.Adjustment(1.0, 1.0, 10.0, 1.0, 1.0, 0)
        self.scale_radius = gtk.HScale(self.adj)
        self.scale_radius.set_digits(0)
        self.scale_radius.set_value(1)
        self.radius = self.scale_radius.get_value()
        self.scale_radius.show()
        self.table.attach(self.scale_radius, 2, 3, 2, 3)
        self.scale_radius.set_visible(False)

        # Pixelate factor label
        self.label_pixelate = gtk.Label("Pixelate factor:")
        self.label_pixelate.show()
        self.table.attach(self.label_pixelate, 1, 2, 2, 3)
        self.label_pixelate.set_visible(False)

        # Pixelate factor scale
        self.adj = gtk.Adjustment(1.0, 1.0, 10.0, 1.0, 1.0, 0)
        self.scale_pixelate = gtk.HScale(self.adj)
        self.scale_pixelate.set_digits(0)
        self.scale_pixelate.set_value(1)
        self.radius = self.scale_pixelate.get_value()
        self.scale_pixelate.show()
        self.table.attach(self.scale_pixelate, 2, 3, 2, 3)
        self.scale_pixelate.set_visible(False)

        # Tile optimalization check button
        self.box_tile_optimalization = gtk.CheckButton("Tile-based processing")
        self.box_tile_optimalization.connect("toggled", self.tile_optimalization_cb)
        self.box_tile_optimalization.set_active(False)
        self.box_tile_optimalization.show()
        self.table.attach(self.box_tile_optimalization, 1, 2, 4, 5)

        # Ok, Cancel and Preview buttons
        self.preview_button = gtk.Button("Preview")
        self.preview_button.show()
        self.dialog.action_area.add(self.preview_button)
        self.ok_button = self.dialog.add_button(gtk.STOCK_OK, gtk.RESPONSE_OK)
        self.cancel_button = self.dialog.add_button(gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)

        # Registering callbacks for buttons
        self.preview_button.connect("clicked", self.preview)
        self.ok_button.connect("clicked", self.ok_clicked)
        self.cancel_button.connect("clicked", self.cancel_clicked)

    # Callback function taking care of changes in UI
    def combobox_on_change(self, widget):
        self.selected_operation = widget.get_active_text()

        # Disable tile optimalization upon combobox change
        tile_optimalization = False
        self.box_tile_optimalization.set_active(False)

        # Gaussian Sharpen -> turn off radius, turn off pixelate factor and turn on tile optimalization
        if self.selected_operation == self.operations[0]:
            self.label_radius.set_visible(False)
            self.scale_radius.set_visible(False)
            self.label_pixelate.set_visible(False)
            self.scale_pixelate.set_visible(False)
            self.box_tile_optimalization.set_visible(True)

        # Pixelate -> turn off radius, turn on pixelate factor and turn on tile optimalization
        elif self.selected_operation == self.operations[5]:
            self.label_radius.set_visible(False)
            self.scale_radius.set_visible(False)
            self.label_pixelate.set_visible(True)
            self.scale_pixelate.set_visible(True)
            self.box_tile_optimalization.set_visible(True)

        # Other filters -> turn on radius, turn off pixelate and turn off tile optimalization
        else:
            self.label_radius.set_visible(True)
            self.scale_radius.set_visible(True)
            self.label_pixelate.set_visible(False)
            self.scale_pixelate.set_visible(False)
            self.box_tile_optimalization.set_visible(False)

    # Same function as OK button, but doesn't merge layers.
    # For future upgrades: possibility to optimize speed with smaller preview
    def preview(self, widget):
        self.convolute()

    # Links GTK checkbox with bool variable used for tile-based processing (ON/OFF)
    def tile_optimalization_cb(self, widget, data = None):
        if widget.get_active():
            self.tile_optimalization = True
        else:
            self.tile_optimalization = False

    # Start convolution process after user presses OK button
    def ok_clicked(self, widget):
        self.convolute()
        pdb.gimp_image_merge_down(self.image, self.layer, 1)

    # Cleanup after cancel button clicked
    def cancel_clicked(self, widget):
        if self.layer is not None:
            self.image.remove_layer(self.layer)
#..........................................................................................................................................#
if __name__ == '__main__':
    convolution_plugin().start()
