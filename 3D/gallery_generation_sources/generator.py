# Sources:

# https://gist.github.com/p2or/a00bdde9f2751940717a404cf977dd01
# https://blender.stackexchange.com/q/57306/3710
# https://blender.stackexchange.com/q/79779/3710

bl_info = {
    "name": "Gallery",
    "description": "Generation of museum gallery.",
    "author": "Marek Klofac",
    "version": (1, 0, 0),
    "blender": (2, 79, 0),
    "location": "3D View > Tools",
    "warning": "",
    "wiki_url": "https://gitlab.com/marek_klofac/bi-pga/blob/master/dokumentace3.adoc",
    "category": "Testing"
}


# ------------------------------------------------------------------------
#    Imports and other Python modules used in this plugin.
# ------------------------------------------------------------------------
import bpy
import os
import math

from bpy.props import (StringProperty,
                       BoolProperty,
                       IntProperty,
                       FloatProperty,
                       EnumProperty,
                       PointerProperty,
                       )
                       
from bpy.types import (Panel,
                       Operator,
                       PropertyGroup,
                       )  

# ------------------------------------------------------------------------
#    Central class of this plugin. Contains all important function
#    that generate gallery.
# ------------------------------------------------------------------------
class Gallery():

    # ------------------------------------------------------------------------
    # Initialize function of this class. Generates empty gallery without any
    # placeholders or historical models. Generates it dependent of properties
    # selected in UI.
    #
    # Important! In generation using ternary operators for selecting source models
    # dependent on gallery style. Maybe a bit confusing, but no need to duplicate code
    def __init__(self, scene, properties, ROOM_OFFSET, PLACEHOLDERS_PER_ROOM, PLACEHOLDERS_POSITIONS, TRASH_BINS_POSITIONS, LIGHT_POSITIONS, SOURCE_MODEL_NAME):
        # Initializing classes internal variables
        self.ROOM_OFFSET = ROOM_OFFSET
        self.PLACEHOLDERS_PER_ROOM = PLACEHOLDERS_PER_ROOM
        self.PLACEHOLDERS_POSITIONS = PLACEHOLDERS_POSITIONS
        self.TRASH_BINS_POSITIONS = TRASH_BINS_POSITIONS
        self.LIGHT_POSITIONS = LIGHT_POSITIONS
        self.SOURCE_MODEL_NAME = SOURCE_MODEL_NAME
        # Preparing the file paths of .blend files from which will be generated
        # the gallery. Taking in consideration the seleceted style in UI.
        # Developed on Windows 10, should work though also on Linux and Mac.
        script_path = bpy.utils.blend_paths()[0]
        root_folder = os.path.dirname(script_path)
        self.PATH = os.path.join(root_folder, "source_models", self.SOURCE_MODEL_NAME)
    
        self.rooms = []

        # Generate starting room
        bpy.ops.wm.append(directory = self.PATH + "\\Object\\", link = False, filename = "start_room")
        self.rooms.append(bpy.data.objects['start_room'])

        # Keep track of how many middle rooms generated
        middle_room_count = 0

        # Doesn't fit into starting and end room, need to generate approriate
        # number of middle rooms and place them in between of starting and ending room.
        # Taking first next integer rounded up with math.ceil()
        if properties.placeholder_count > 2*self.PLACEHOLDERS_PER_ROOM:

            middle_room_count = math.ceil((properties.placeholder_count - 2*self.PLACEHOLDERS_PER_ROOM) / self.PLACEHOLDERS_PER_ROOM)
            # Append models to be copied from
            if int(properties.gallery_style) == 1:
                bpy.ops.wm.append(directory = self.PATH + "\\Object\\", link = False, filename = "even_middle_room")
                bpy.ops.wm.append(directory = self.PATH + "\\Object\\", link = False, filename = "odd_middle_room")
            else:
                bpy.ops.wm.append(directory = self.PATH + "\\Object\\", link = False, filename = "middle_room")

            # Spiral style - on odd indices generate odd rooms and other wise
            # Hallway style - generate just middle room
            for i in range(middle_room_count):
                # Generate odd middle room
                if (i % 2):
                    # Copy
                    self.rooms.append(bpy.data.objects['odd_middle_room' if int(properties.gallery_style) == 1 else "middle_room"].copy())
                    # Link
                    scene.objects.link(self.rooms[-1])
                    # Translate - in first iteration accessing Y value of starting room
                    self.rooms[-1].location.y = self.rooms[-2].location.y + self.ROOM_OFFSET

                # Generate even middle room
                else:
                    # Copy
                    self.rooms.append(bpy.data.objects['even_middle_room' if int(properties.gallery_style) == 1 else "middle_room"].copy())
                    # Link
                    scene.objects.link(self.rooms[-1])
                    # Translate
                    self.rooms[-1].location.y = self.rooms[-2].location.y + self.ROOM_OFFSET

            # Deleting appended middle rooms. Clean up.
            bpy.ops.object.select_all(action='DESELECT')
            if int(properties.gallery_style) == 1:
                bpy.data.objects['even_middle_room'].select = True
                bpy.data.objects['odd_middle_room'].select = True
            else:
                bpy.data.objects['middle_room'].select = True
            bpy.ops.object.delete()

        # Generate approriate ending room (this could be written as one-liner with two ternary operators nested)
        if int(properties.gallery_style) == 1:
            model = "odd_end_room" if middle_room_count % 2 else "even_end_room"

        else:
            model = "end_room"

        bpy.ops.wm.append(directory = self.PATH + "\\Object\\", link = False, filename = model)
        self.rooms.append(bpy.data.objects[model])
        self.rooms[-1].location.y = self.rooms[-2].location.y + self.ROOM_OFFSET

        # At the end, generate appropriate number of props in each room
        if properties.trash_bin:
            self.generate_props(scene, properties);

    # ------------------------------------------------------------------------
    # This function generates props like trash cans etc into the gallery
    # dependent on UI selected values.
    def generate_props(self, scene, properties):
        self.trash_bins = []
        # Append source props from which to copy
        bpy.ops.wm.append(directory = self.PATH + "\\Object\\", link = False, filename = "trash_bin")
            
        for room in self.rooms:
            # Append
            self.trash_bins.append(bpy.data.objects['trash_bin'].copy())
            # Link
            scene.objects.link(self.trash_bins[-1])
            # Translate
            index = self.rooms.index(room);
            self.trash_bins[-1].location.x = self.TRASH_BINS_POSITIONS[index % 2][0]
            self.trash_bins[-1].location.y = self.TRASH_BINS_POSITIONS[index % 2][1] + index * self.ROOM_OFFSET

        # Delete appended props from which to copy
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects['trash_bin'].select = True
        bpy.ops.object.delete()

    # ------------------------------------------------------------------------
    # This function generates placeholders respectively to UI properties.
    def generate_placeholders(self, scene, properties):
        # Save placeholders also in a list, for later use with historical artifacts
        self.placeholders = []

        # Choose the right pedestal dependent on UI properties (ternary operators to shorten)
        model = "modern_pedestal" if int(properties.placeholder_type) == 1 else "rock_pedestal" if int(properties.placeholder_type) == 2 else "wooden_pedestal"

        # Append the source pedestal fro which will be copied from. Later deleted.
        bpy.ops.wm.append(directory = self.PATH + "\\Object\\", link = False, filename = model)
        
        # Number of rooms, that will be filled with some number of placeholders
        full_filled_rooms = math.floor(properties.placeholder_count / self.PLACEHOLDERS_PER_ROOM)

        # First generate placeholders in rooms, that will be fully occupied
        for room in range(full_filled_rooms):
            for pos in range(self.PLACEHOLDERS_PER_ROOM):
                # Copy
                self.placeholders.append(bpy.data.objects[model].copy())
                # Link
                scene.objects.link(self.placeholders[-1])
                # Scale, if hallway style
                if int(properties.gallery_style) != 1:
                    self.placeholders[-1].scale[2] *= properties.placeholder_height
                # Translate - here offseting only in Y axis, the same as rooms...
                self.placeholders[-1].location.x = self.PLACEHOLDERS_POSITIONS[pos][0]
                self.placeholders[-1].location.y = self.PLACEHOLDERS_POSITIONS[pos][1] + room * self.ROOM_OFFSET

        # Then generate placeholders in last room, that will be only partially filled.
        for pos in range(properties.placeholder_count % self.PLACEHOLDERS_PER_ROOM):
            # Copy
            self.placeholders.append(bpy.data.objects[model].copy())
            # Link
            scene.objects.link(self.placeholders[-1])
            # Translate - here using full_filled_rooms as offset multiplier to "access" the last room
            self.placeholders[-1].location.x = self.PLACEHOLDERS_POSITIONS[pos][0]
            self.placeholders[-1].location.y = self.PLACEHOLDERS_POSITIONS[pos][1] + full_filled_rooms * self.ROOM_OFFSET

        # In case of hallway style, sort out the offset from UI
        if int(properties.gallery_style) != 1:
            self.offset_placeholders(properties)

        # Deleting appended source pedestal
        bpy.ops.object.select_all(action='DESELECT')
        bpy.data.objects[model].select = True
        bpy.ops.object.delete()

    # ------------------------------------------------------------------------
    #  This function moves pre-generated placeholders closer together or
    #  further away from each other, dependent on UI properties
    def offset_placeholders(self, properties):
        # Placeholders were generated in order rigt, left, right, left...
        # Due to PLACEHOLDERS_POSITIONS order. We want move placeholders only
        # in their appropriate sides, not across the room. Using array slicing.
        for index, placeholder in enumerate(self.placeholders[0::2]):
            if index == 0:
                continue
            placeholder.location.y = self.placeholders[0::2][index-1].location.y + properties.placeholder_offset
        
        for index, placeholder in enumerate(self.placeholders[1::2]):
            if index == 0:
                continue
            placeholder.location.y = self.placeholders[1::2][index-1].location.y + properties.placeholder_offset

    # ------------------------------------------------------------------------
    #  This function places two lamps into each room of gallery.
    def generate_lights(self):
        for room in self.rooms:
            self.LIGHT_POSITIONS
            # Adding first lamp relative to rooms coordinates
            bpy.ops.object.lamp_add(
                type='POINT',
                radius=1,
                view_align=False,
                location=(room.location.x + self.LIGHT_POSITIONS[0][0],
                          room.location.y + self.LIGHT_POSITIONS[0][1], 
                          room.location.z + self.LIGHT_POSITIONS[0][2]),
                layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False)
            )
            # Adding second lamp relative to rooms coordinates
            bpy.ops.object.lamp_add(
                type='POINT',
                radius=1,
                view_align=False,
                location=(room.location.x + self.LIGHT_POSITIONS[1][0],
                          room.location.y + self.LIGHT_POSITIONS[1][1], 
                          room.location.z + self.LIGHT_POSITIONS[1][2]),
                layers=(True, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False, False)
            )

    # ------------------------------------------------------------------------
    # This function distributes given historic artifact models by user
    # and places them onto pre-generated placeholders
    def distribute_user_models(self, models, properties):
        # Here iterating over two lists. Zip truncates to shorter list
        for model, placeholder in zip(models, self.placeholders):
            # Set model's origin to the origin of geometry, using bounds as center
            bpy.ops.object.select_all(action='DESELECT')
            model.select = True
            bpy.ops.object.origin_set(type='ORIGIN_GEOMETRY', center='BOUNDS')

            # Set appropriate scale of model to placeholder, including selected size from UI
            scale_factor = (placeholder.dimensions[0] / model.dimensions[0]) * properties.artifact_size

            # Scaling uniformly along all axis
            bpy.ops.transform.resize(
                value=(scale_factor, scale_factor, scale_factor),
                constraint_axis=(False, False, False),
                constraint_orientation='GLOBAL',
                mirror=False,
                proportional='DISABLED',
                proportional_edit_falloff='SMOOTH',
                proportional_size=1
            )
            # Snap model to placeholder
            model.location = placeholder.location
            # Modify Z-axis - calculate with own model's origin offset + placeholder offset
            model.location.z += model.dimensions[2]/2 + placeholder.dimensions[2]


# ------------------------------------------------------------------------
#    User interface elements - buttons, checkboxes, comboboxes, etc.
# ------------------------------------------------------------------------
class UI_Properties(PropertyGroup):
    #
    # Independent properties on gallery style
    #
    
    gallery_style = bpy.props.EnumProperty(
        items = [
            ("1", "Spiral", "Spiral style of gallery"),
            ("2", "Hallway", "Hallway style of gallery")
        ],
        name = "Gallery style",
        description = "Choose a style of museum gallery that will be generated.",
        default = None
    )
    
    objects_folder_path = StringProperty(
        name = "Folder with historical objects",
        description="Choose a directory that contains .blend files with historical artifacts, you want to be present in the gallery.",
        default="",
        maxlen=1024,
        subtype='DIR_PATH'
    )
    
    placeholder_count = bpy.props.IntProperty(
        name = "Number of placeholders",
        description = "Total number of pedestals for histroical artifacts. The size of generated museum gallery is dependant on this value.",
        default = 8, min = 1, max = 1000, step = 1
    )
    
    artifact_size = bpy.props.FloatProperty(
        name = "Historical object size",
        description = "Scale factor for all imported historical artifacts.",
        default = 1.0, min = 0, max = 10.0, step = 0.1
    )
    
    placeholder_type = bpy.props.EnumProperty(
        items = [
            ("1", "Modern pedestal", "Simple modern and white pedestal."),
            ("2", "Rock", "Old greek rock."),
            ("3", "Wooden table", "Small wooden round table.")
        ],
        name = "Placeholder style",
        description = "Choose a style of museum gallery that will be generated.",
        default = None
    )

    #
    # Dependent properties on selected gallery style
    #
    
    # Spiral style
    trash_bin = bpy.props.BoolProperty(
        name = 'Trash bins', 
        description = 'Adds trash bins into the gallery',
        default = False
    )
    
    # Hallway style

    placeholder_height = bpy.props.FloatProperty(
        name = "Placeholder height",
        description = "The height of pedestal or display case.",
        default = 1.0, min = 0, max = 2.0, step = 0.1
    )

    placeholder_offset = bpy.props.FloatProperty(
        name = "Placeholder offset",
        description = "The space between placeholders.",
        default = 5.0, min = 1.0, max = 5.0, step = 0.1
    )


# ------------------------------------------------------------------------
#    UI button opetaro that starts the gallery generation upon click
# ------------------------------------------------------------------------
class UI_Operator_Generate(bpy.types.Operator):
    bl_label = "Generate!"
    bl_idname = "wm.generate"

    # ------------------------------------------------------------------------
    # Upon click, start this
    def execute(self, context):
        bpy.ops.object.select_all(action='DESELECT')

        # Setting parameters and options for selected gallery style
        # in UI. Each style is different.
        #
        # Spiral style
        if int(context.scene.properties.gallery_style) == 1:
            ROOM_OFFSET = 5.03
            PLACEHOLDERS_PER_ROOM = 4
            PLACEHOLDERS_POSITIONS = [
                (-3.5, 0.7),
                (-6.5, 0.7),
                (-6.5, 4.3),
                (-3.5, 4.3)
            ]
            TRASH_BINS_POSITIONS = [
                (-0.3, 0.5),
                (-0.3, 4.5)
            ]
            LIGHT_POSITIONS = [
                (-8.5, 2.5, 2),
                (-1.5, 2.5, 2)
            ]
            PATH = "spiral_style.blend"
        # Hallway style
        else:
            ROOM_OFFSET = 10.0
            PLACEHOLDERS_PER_ROOM = 4
            PLACEHOLDERS_POSITIONS = [
                (-0.6, 3.0),
                (-4.6, 3.0),
                (-0.6, 7.0),
                (-4.6, 7.0)
            ]
            TRASH_BINS_POSITIONS = [
                (0.0, 0.0),
                (0.0, 0.0)
            ]
            LIGHT_POSITIONS = [
                (-2.5, 2.5, 2.0),
                (-2.5, 7.5, 2.0)
            ]
            PATH = "hallway_style.blend"

        # Creating Gallery object
        gallery = Gallery(
            context.scene,
            context.scene.properties,
            ROOM_OFFSET,
            PLACEHOLDERS_PER_ROOM,
            PLACEHOLDERS_POSITIONS,
            TRASH_BINS_POSITIONS,
            LIGHT_POSITIONS,
            PATH
        )
        # Then distributing placeholders appropriately through out the
        # museum gallery.
        gallery.generate_placeholders(context.scene, context.scene.properties);

        # Generate lights in rooms through out the gallery
        gallery.generate_lights()

        self.import_user_models(context.scene.properties)

        # If not empty, properly distribute loaded users models in the museum gallery
        # and put them onto generated placeholders
        if self.user_models:
            gallery.distribute_user_models(self.user_models, context.scene.properties)

        return {'FINISHED'}

    # ------------------------------------------------------------------------
    # This function loads and appends to current scene the models given by the user
    # in "Folder with historical models" UI string property.
    # Returns and array of these objects.
    def import_user_models(self, properties):
        self.user_models = []
        # String not empty
        if properties.objects_folder_path:
            for file in os.listdir(properties.objects_folder_path):
                # For all *.blend files append from them object called "model" to the scene
                # Artifacts has to be called "model". We don't want to import all objects
                # from another scene, just the historical artifact
                if file.endswith(".blend") and len(self.user_models) < properties.placeholder_count:
                    # IMPORTANT!
                    #
                    # Using Blender Render for this script. Given historical 3D models that
                    # are in Cycles will not work. Please supply Blender Render models only!
                    bpy.ops.wm.append(
                        directory = os.path.join(properties.objects_folder_path, file) + "\\Object\\",
                        link = False,
                        filename = "model"
                    )
                    # Adding to output array
                    if not self.user_models:
                        self.user_models.append(bpy.data.objects['model'])
                    # Blender's convention of naming same objects -> model.001, model.002, etc.
                    else:
                        self.user_models.append(bpy.data.objects['model.' + str(len(self.user_models)).zfill(3)])


# ------------------------------------------------------------------------
#    UI button operator that undoes previous operation upon click
# ------------------------------------------------------------------------
class UI_Operator_Undo(bpy.types.Operator):
    bl_label = "Undo (works same as ctrl+z)"
    bl_idname = "wm.undo"

    # ------------------------------------------------------------------------
    # Upon click, start this
    def execute(self, context):
        bpy.ops.ed.undo()
        return {'FINISHED'}


# ------------------------------------------------------------------------
#    Panel in 3D view, using predefined UI elements in class UI_Properties
# ------------------------------------------------------------------------
class UI_Panel(Panel):
    bl_label = "Museum gallery generation"
    bl_idname = "OBJECT_PT_custom_panel"
    bl_space_type = "VIEW_3D"   
    bl_region_type = "TOOLS"    
    bl_category = "Tools"
    bl_context = "objectmode"   

    def draw(self, context):
        layout = self.layout
        scene = context.scene
        properties = scene.properties
        
        # Indepentent UI elements
        layout.prop(properties, "gallery_style")
        layout.prop(properties, "objects_folder_path") 
        layout.prop(properties, "placeholder_count")
        layout.prop(properties, "artifact_size")
        layout.prop(properties, "placeholder_type")
        
        # Dependent UI elements on selected Gallery style
        # Spiral style selected
        if int(properties.gallery_style) == 1:
            layout.prop(properties, "trash_bin")
            
        # Hallway style selected
        else:
            layout.prop(properties, "placeholder_height")
            layout.prop(properties, "placeholder_offset")
        
        # Generate button
        layout.operator("wm.generate")

        # Undo button
        layout.operator("wm.undo")


# ------------------------------------------------------------------------
#    Blender registration
# ------------------------------------------------------------------------
def register():
    bpy.utils.register_module(__name__)
    bpy.types.Scene.properties = PointerProperty(type=UI_Properties)

def unregister():
    bpy.utils.unregister_module(__name__)
    del bpy.types.Scene.properties

if __name__ == "__main__":
    register()