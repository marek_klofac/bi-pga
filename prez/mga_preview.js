//
// MGA preview testing app for SAGE2 presentation
// by: Jiri Kubista <kubista@cesnet.cz> <kubisjir@fit.cvut.cz>
//
// Copyright (c) 2018
//
// Disclaimer: This is BETA version.
// Please send feedback to <kubisjir@fit.cvut.cz> with subject "MGA preview".
//

"use strict";

let timer = 0;
let wall_width = 9600;
let wall_height = 4320;
let sources = {};
let auto_updates = [];
let manual_updates = [];
let dir_path = "";
let layouts = [];
let current_layout = 0;
let dir = "";
let pause = false;

window.onload = function(){
	// load_config();
	let inputElement = document.getElementById("config_input");
	inputElement.addEventListener("change", load_file, false);
    $("#dir_input").on("keypress keydown keyup", function(e) {
        e.stopPropagation();
    });
	setInterval(update,1000);
	clear_wall();
}

window.onresize = function(event) {
    let wall = $("#wall");
    wall.height(wall.width()*0.45);
};

document.onkeydown = event_handler;

function configure_wall(config){
	clear_wall();
    config.sectors.forEach(function(sector, i){
		let div = document.createElement("div");
		div.style.position = "absolute";
		div.style.width = ((sector.size.width/wall_width)*100) + "%";
        div.style.height = ((sector.size.height/wall_height)*100) + "%";
        div.style.left = ((sector.position.x/wall_width)*100) + "%";
        div.style.top = ((sector.position.y/wall_height)*100) + "%"
		
		let img = document.createElement("img");
		// img.style.position = "absolute";
		let id = "sector_" + i;
		img.setAttribute("id", id);;
		img.style.width = "100%";
        img.style.height = "100%";
		img.style.objectFit = "contain";
        load_images(id, sector);
        img.src = sources[id][0];       	
        
        div.append(img);
        wall.append(div);
        if (sector.timing === 0){
           	manual_updates.push({
           	    "id": id,
           	    "current": 0,
           	    "loop": sector.loop
           	});
        } else {
           	auto_updates.push({
           	    "id": id,
           	    "timing": sector.timing,
           	    "current": 0,
           	    "loop": sector.loop
           	});
        }
	});
}

function load_images(id, sector){
    sources[id] = [];
    sector.images.forEach(function(image){
        sources[id].push(dir_path + sector.path + "/" + image);
    });
}

function update(){
    if (pause) return;
    timer++;
    auto_updates.forEach(function(sector){
        if(timer % sector.timing === 0){
            next_picture(sector);
        }
    });
}

function next_picture(sector){
    if (sector.loop || ((sector.current + 1) < sources[sector.id].length)){
        sector.current = (sector.current + 1) % sources[sector.id].length;
        $("#" + sector.id).attr("src", sources[sector.id][sector.current]); 
    }
}

function prev_picture(sector){
    if (sector.loop || ((sector.current) > 0)){
        let n = sector.current;
        let m = sources[sector.id].length;
        sector.current = ((n - 1 % m) + m) % m;
        $("#" + sector.id).attr("src", sources[sector.id][sector.current]);
    }
}

function event_handler(e){
    let code = e.keyCode;
    if (code == 34 || code == 39 || code == 40){
        // Page Down, Right, Down
        manual_updates.forEach(function(sector){
            next_picture(sector);
        });
        e.preventDefault();
    }else if (code == 33 || code == 37 || code == 38){
        // Page Up, Left, Up
        manual_updates.forEach(function(sector){
            prev_picture(sector);
        });
        e.preventDefault();
    }else if (code == 83){
        // S
        pause = !pause;
        $("#pause").toggle();
    } else if (code == 80){
    	// P
    	// $("#config_input").click();
    } else if (code == 78){
    	// N
    	// $("#config_input").click();
    }
}

function load_file() {
	clear_wall();
	dir_path = $("#dir_input").val() + "/";
 	let config;
	let fr = new FileReader();
    fr.onload = (e) => {
    	config = JSON.parse(e.target.result);
    	configure_wall(config);
    };
    try{
        fr.readAsText(this.files[0]);
    }
    catch(err){
        console.log(err.message);
    } 
}

function clear_wall(){
	sources = {};
	auto_updates = [];
	manual_updates = [];
	timer = 0;
	let wall = $("#wall");
	wall.empty();
    wall.height(wall.width()*0.45);
    create_grid();
}

function create_grid(){
	for (var i = 0; i < 5; i++){
		for (var j = 0; j < 4; j++){
			let div = document.createElement("div");
    		div.style.left = i * 20 + "%";
    		div.style.top = j * 25 + "%";
    		div.setAttribute("class", "grid");
    		$("#wall").append(div);
		}
	}
}