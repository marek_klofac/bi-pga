# Rozcestnik

## 2D

Tema: Konvoluce 2

Popis: Prace s obrazem, plugin realizujici zaostreni, rozostreni a pixelizaci

Body: 15

image::img1.jpg[1]

** xref:/2D/dokumentace1.adoc#[2D doc]

## 3D

Tema: Generovani objektu

Popis: Generaci muzejni galerie dle danych parametru. Zaplneni galerie poskytnutymi uzivatelskymi exponaty.

Body: 40

image::img3.jpg[width="75%"]

** xref:/3D/dokumentace3.adoc#[3D Doc]

## Vizualizace

Tema: Vizualizace vetrnych proudu

Popis: SAGE2 aplikace vyuzivajici Windy API pro vizualizaci real-time meteorologickych dat.

Body: 40

image::img2.jpg[width="75%"]

** xref:/SAGE2/dokumentace2.adoc#[SAGE2 Doc]
